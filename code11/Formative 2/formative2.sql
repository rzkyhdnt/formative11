use staging
create table Product_ClientA(
id int not null auto_increment,
name varchar(20),
description varchar(50),
artNumber int,
price int,
stock int,
deleted boolean,
brandName varchar(20),
primary key(id))

create table Product_ClientB(
id int not null auto_increment,
nama varchar(20),
deskripsi varchar(50),
nomorArtikel int,
harga int,
persediaanBarang int,
dihapus boolean,
namaMerk varchar(20),
primary key(id))

use production
create table product(
id int not null auto_increment,
name varchar(20),
description varchar(50),
artNumber int,
price int,
stock int, 
isDelete boolean,
brand varchar(10),
primary key(id))