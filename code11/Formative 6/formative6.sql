insert into brand(name)
select brandName from staging.product_clienta;

insert into product(name, description, artNumber, price, stock, isDelete, brandName)
select name, description, artNumber, price, stock, deleted, brandName from staging.product_clienta;

select * from product;

use production;

insert into product(name, description, artNumber, price, stock, isDelete, brandName)
select nama, deskripsi, nomorArtikel, harga, persediaanBarang, dihapus, namaMerk from staging.product_clientb;

alter table product add brand_id int not null;

select * from brand;

update product set product.brand_id = 
(select brand.id from brand where product.brandName = brand.name) where id > 0;

select * from product;