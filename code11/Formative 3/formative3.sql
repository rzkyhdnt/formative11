insert into product_clienta(name, description, artNumber, price, stock, deleted, brandName) values 
("Nike 360", "Sepatu stylish berlogo ceklis", 111224, 720000, 2, true, "Nike"),
("Diadora Posture", "Sepatu diperuntukan untuk teenager", 112411, 800000, 1, true, "Diadora"),
("Puma Starling", "Sepatu classic berlogo jaguar", 112221, 720000, 2, false, "Puma"),
("Vans Extreme", "Sneaker dengan tipe gaya dari Vans", 111372, 710000, 3, false, "Vans"),
("Adidas Airmax", "Sepatu casual cocok untuk kawula muda", 111233, 980000, 1, false, "Adidas"),
("Airwalk Vancouver", "Senakers murah branded lokal", 111254, 500000, 4, false, "Airwalk"),
("New Balance 990", "Sepatu oldskool", 111261, 900000, 1, false, "New Balance"),
("Nike Airmax", "Sepatu berlogo ceklis", 111224, 450000, 3, false, "Nike"),
("New Balance 574", "Sepatu dengan model terbaru", 111261, 890000, 2,  true, "New Balance"),
("Adidas Airmax 5", "Sepatu casual cocok untuk kawula muda", 111233, 1000000, 1, false, "Adidas");

insert into product_clientb(nama, deskripsi, nomorArtikel, harga, persediaanBarang, dihapus, namaMerk) values 
("Airwalk Kristop", "Senakers murah branded lokal", 111254, 450000, 6, false, "Airwalk"),
("Nike Air Jordan", "Sepatu stylish berlogo ceklis", 111224, 750000, 3, false, "Nike"),
("Adidas Yeezy", "Sepatu casual cocok untuk kawula muda", 111233, 800000, 2, false, "Adidas"),
("Puma Suede", "Sepatu classic berlogo jaguar", 112221, 650000, 4, false, "Puma"),
("Diadora N9000", "Sepatu diperuntukan untuk tennis", 112411, 940000, 3, true, "Diadora"),
("Nike Airmax", "Sepatu berlogo ceklis", 111224, 450000, 3, false, "Nike"),
("Vans Old Skool", "Sneaker dengan tipe gaya oldskool dari Vans", 111372, 600000, 2, false, "Vans"),
("New Balance 574", "Sepatu dengan model terbaru", 111261, 890000, 2,  false, "New Balance"),
("New Balance 990", "Sepatu oldskool", 111261, 900000, 1, false, "New Balance"),
("Puma X", "Sepatu classic berlogo jaguar", 112221, 720000, 2, false, "Puma")
